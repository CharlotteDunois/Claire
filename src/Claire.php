<?php
/**
 * Claire
 * Copyright 2018 Charlotte Dunois, All Rights Reserved
 *
 * Website: https://charuru.moe
 * License: https://github.com/CharlotteDunois/Claire/blob/master/LICENSE
*/

namespace CharlotteDunois\Claire;

/**
 * The huggable class.
 */
class Claire implements \Psr\Hug\Huggable, \Psr\Hug\GroupHuggable {
    protected $hugged = array();
    
    /**
     * {@inheritdoc}
     * @return void
     */
    function hug(\Psr\Hug\Huggable $h): void {
        $id = \spl_object_id($h);
        
        if(isset($this->hugged[$id])) {
            unset($this->hugged[$id]);
            return;
        }
        
        $this->hugged[$id] = true;
        $h->hug($this);
    }
    
    /**
     * {@inheritdoc}
     * @return void
     * @throws \InvalidArgumentException
     */
    function groupHug($huggables): void {
        foreach($huggables as $h) {
            if(!($h instanceof \Psr\Hug\Huggable)) {
                throw new \InvalidArgumentException('Unhuggable object given');
            }
            
            $h->hug($this);
        }
    }
}
