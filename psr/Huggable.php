<?php
/**
 * Claire
 * Copyright 2018 Charlotte Dunois, All Rights Reserved
 *
 * Website: https://charuru.moe
 * License: https://github.com/CharlotteDunois/Claire/blob/master/LICENSE
*/

/**
 * This interface is taken from https://github.com/php-fig/fig-standards/blob/master/proposed/psr-8-hug/psr-8-hug.md and a semantically updated.
 */

namespace Psr\Hug;

/**
 * Defines a huggable object.
 * A huggable object expresses mutual affection with another huggable object.
 */
interface Huggable {
    /**
     * Hugs this object.
     * All hugs are mutual. An object that is hugged MUST in turn hug the other
     * object back by calling hug() on the first parameter. All objects MUST
     * implement a mechanism to prevent an infinite loop of hugging.
     *
     * @param Huggable  $h  The object that is hugging this object.
     * @return void
     */
    function hug(Huggable $h): void;
}
