# Claire [![Build Status](https://scrutinizer-ci.com/gl/CharlotteDunoisLabs/CharlotteDunois/Claire/badges/build.png?b=master&s=ee7ad79008611769777a60c9fa98c67650273619)](https://scrutinizer-ci.com/gl/CharlotteDunoisLabs/CharlotteDunois/Claire/build-status/master)

Claire is a PSR-8 compatible library.

# Getting Started
Getting started with Yasmin is pretty straight forward. All you need to do is to use [composer](https://packagist.neko.run/packages/charlottedunois/claire) to install Claire. After that, you can include composer's autoloader into your file and start using Claire!

```
composer require charlottedunois/claire
```
